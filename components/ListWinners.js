import React, { useEffect, useState } from 'react';
import { ScrollView } from 'react-native';
import { Header, ListItem } from 'react-native-elements';

import { getWinners } from './../api';

const ListWinners = ({ token, resetToken }) => {
  const [list, setList] = useState([]);

  const populateList = (winners) => {
    setList(winners.slice(0));
  };

  useEffect(() => {
    getWinners(token).then((response) => {
      if (response.ok) {
        response.json().then((json) => populateList(json.winners));
      } else {
        console.log('Error');
      }
    });
  }, []);

  return (
    <>
      <Header
        centerComponent={{
          text: 'FIFA Winners',
          style: {
            color: '#fff',
            fontFamily: 'Helvetica',
            fontSize: 20,
            fontWeight: 'bold',
            padding: 10,
          },
        }}
        rightComponent={{
          icon: 'logout',
          color: '#fff',
          onPress: resetToken,
          iconStyle: { padding: 10 },
        }}
      />
      <ScrollView>
        {list.map((winner, i) => (
          <ListItem key={i} bottomDivider>
            <ListItem.Content>
              <ListItem.Title>{winner.country}</ListItem.Title>
              <ListItem.Subtitle>{winner.year}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        ))}
      </ScrollView>
    </>
  );
};

export default ListWinners;
