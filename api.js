const BASE_URL = 'https://frozen-peak-68797.herokuapp.com';

export const postLogin = (email, password) => {
  return window.fetch(`${BASE_URL}/login`, {
    headers: { 'content-type': 'application/json' },
    method: 'POST',
    body: JSON.stringify({ email, password }),
  });
};

export const getWinners = (token) => {
  return window.fetch(`${BASE_URL}/winners`, {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
};
