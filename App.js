import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import Login from './components/Login';
import ListWinners from './components/ListWinners';

export default function App() {
  const [token, setToken] = useState();

  return (
    <SafeAreaProvider>
      <View style={styles.container}>
        {!!token ? (
          <ListWinners token={token} resetToken={() => setToken(null)} />
        ) : (
          <Login setToken={setToken} />
        )}
      </View>
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
  },
});
