import React, { useState } from 'react';
import { View } from 'react-native';
import { Header, Input } from 'react-native-elements';
import { Button } from 'react-native-elements';
import { postLogin } from './../api';

const Login = ({ setToken }) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const submitLogin = () => {
    setIsLoading(true);
    postLogin(email, password)
      .then((response) => {
        if (response.ok) {
          response
            .json()
            .then((json) => {
              setToken(json.Token);
            })
            .catch(() => {
              setErrorMessage('Something went wrong');
              console.log({ message: 'Error reading response' });
            });
        } else {
          setPassword('');
          setErrorMessage('Invalid login');
          setTimeout(() => {
            setErrorMessage('');
          }, 3000);
          console.log({ message: 'Unauthorized' });
        }
      })
      .finally(() => setIsLoading(false));
  };

  const onPressHandler = () => {
    console.log('pressed');
    submitLogin();
  };

  return (
    <>
      <Header
        centerComponent={{
          text: 'Log in',
          style: {
            color: '#fff',
            fontFamily: 'Helvetica',
            fontSize: 20,
            fontWeight: 'bold',
            padding: 10,
          },
        }}
      />
      <View style={{ marginTop: 20 }}>
        <Input
          placeholder="E-mail"
          leftIcon={{ type: 'font-awesome', name: 'envelope' }}
          keyboardType="email-address"
          style={{ padding: 10 }}
          onChangeText={setEmail}
        />

        <Input
          placeholder="Password"
          errorStyle={{ color: 'red' }}
          leftIcon={{ type: 'font-awesome', name: 'key' }}
          secureTextEntry={true}
          style={{ padding: 10 }}
          value={password}
          onChangeText={setPassword}
          errorMessage={errorMessage}
          errorStyle={{ textAlign: 'center' }}
        />

        <Button
          title="Log in"
          style={{ padding: 10, width: 200, alignSelf: 'center' }}
          onPress={onPressHandler}
          disabled={isLoading}
          loading={isLoading}
        />
      </View>
    </>
  );
};

export default Login;
